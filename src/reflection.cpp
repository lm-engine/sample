#include "enemy_component.h"
#include "protagonist_component.h"

#include <lmengine/reflection.h>

void reflect_types_cpp()
{
    REFLECT_TYPE(protagonist_component, "Protagonist")
      .REFLECT_MEMBER(protagonist_component, move_force, "Move force")
      .REFLECT_MEMBER(protagonist_component, jump_impulse, "Jump impulse");

    REFLECT_TYPE(enemy_component, "Enemy");
}
